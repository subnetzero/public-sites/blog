---
title: "About"
date: 2018-09-01
layout: "about"
slug: "me"
comments: false
draft: true
---

### Who
Q: _What's big and white and shaped like a tooth?_

A: _A molar bear!_

That joke sums me up as a person. But read on for more detail...

I'm Fletcher! My favorite color is blue, and my cat's name is Perceval Grimwhisker. I've been a software engineer, devops engineer, sysadmin, and various other titles for awhile now. I have a consultancy (https://subnetzero.io) for entities who need some help with cloud infrastructure, scaling, reliability, migration, best practices, etc.

### What
This blog's purpose is to provide a place for high-quality, advanced tutorials in various *_practical_* areas of computer science, networking, big data, machine learning, and all those other buzzwordy areas.

Almost all of the tutorials use Linux-based, open-source tools. Not because you can't do similar things with MS tools, but because it's what I am most familiar with.

Oh, and the MS stuff isn't cheap.

### Why
I believe there's a fundamental issue with learning to design and build systems at large scale: most of the learning is done on-the-job in an apprenticeship style model. Or, if your start-up/company/whatever experiences sudden growth, you have to learn on the job. Most of the tutorials I've found cover very small-scale usage of a system, or they just rehash the _Quickstart_ section. This is understandable; it's expensive and time-consuming to build large-scale systems and write a tutorial about it.

This blog is my attempt to make gaining experience with these concepts and putting them into practice a bit easier.

### How
By providing high-quality, advanced tutorials in *_practical_* areas of computer science, networking, big data, and machine learning. The tutorials are written from the perspective of designing, implementing and growing a specific _product_ rather than a single _technology_. Said another way, you might find a tutorial on building a data pipeline on here, but probably not a tutorial on starting a single Kafka node.

### Where
It's not _quite_ true that you have to spend a lot of money to get experience in these areas. The tutorials all assume a shoestring budget, and we'll do what we can with your local laptop and free resources. Where we have to do something that costs additional money, I'll do my best to call it out.

As a general rule, we build things to be as platform-agnostic (in terms of Cloud Provider) as possible, so they can run in AWS, GCP, Alicloud, DigitalOcean and anywhere else. If we have to use something specific to a cloud provider, I'll call it out.

### End

I hope you enjoy these tutorials and have fun going through them! If you have questions, feel free to e-mail me, post on the Discourse forum, or join one of the many chat services. Want to check out the tutorials? Go here: https://blog.subnetzero.io/tutorials/.
