==================================================================
https://keybase.io/subnetzero
--------------------------------------------------------------------

I hereby claim:

  * I am an admin of https://blog.subnetzero.io
  * I am subnetzero (https://keybase.io/subnetzero) on keybase.
  * I have a public key ASDcOKXALeB0k6kTaRxa7_Fgz6S-mz9XUdZa9pE6u1TBCAo

To do so, I am signing this object:

{
  "body": {
    "key": {
      "eldest_kid": "0120dc38a5c02de07493a913691c5aeff160cfa4be9b3f5751d65af6913abb54c1080a",
      "host": "keybase.io",
      "kid": "0120dc38a5c02de07493a913691c5aeff160cfa4be9b3f5751d65af6913abb54c1080a",
      "uid": "9cdedf54a3a3abf0d0bc83bf6bf18019",
      "username": "subnetzero"
    },
    "merkle_root": {
      "ctime": 1539192776,
      "hash": "d57f1bcd90270cd4215f2edccf0017933f0722295dc70a3c4b86527eab21cebaac420eedde9dcee4ffbdbbaa95feb9622204ad8570a31e0958f3d3bbdede5ab9",
      "hash_meta": "ba2fcff66e1fb7a74032e34a28b075ea1e90f719ec880ed70ab90795c7d24868",
      "seqno": 3776487
    },
    "service": {
      "entropy": "0hBB/AFtsQ86Ezz/LQebbxr5",
      "hostname": "blog.subnetzero.io",
      "protocol": "https:"
    },
    "type": "web_service_binding",
    "version": 2
  },
  "client": {
    "name": "keybase.io go client",
    "version": "2.7.3"
  },
  "ctime": 1539192791,
  "expire_in": 504576000,
  "prev": "6b00cbaed6a20786b4a73bb2fe80c31a374d5515d44836685aa0f034ac8c3085",
  "seqno": 16,
  "tag": "signature"
}

which yields the signature:

hKRib2R5hqhkZXRhY2hlZMOpaGFzaF90eXBlCqNrZXnEIwEg3DilwC3gdJOpE2kcWu/xYM+kvps/V1HWWvaROrtUwQgKp3BheWxvYWTESpcCEMQgawDLrtaiB4a0pzuy/oDDGjdNVRXUSDZoWqDwNKyMMIXEIPnzt/0l/Ovjy1QO69Hi9+y6YrwRup8/GM5mc1YivwCXAgHCo3NpZ8RAxvLkYaSczVT0ygZu3LFpxHwddzfQbFPhoygcqQzJCItbYCl0G8T7I44zXR73iFhwzLcjELiuk+t0LmAdWsSJAKhzaWdfdHlwZSCkaGFzaIKkdHlwZQildmFsdWXEIEtrT1c8QpyQVpyNV50AzaVTebFnTikCG+dv52iumI6lo3RhZ80CAqd2ZXJzaW9uAQ==

And finally, I am proving ownership of this host by posting or
appending to this document.

View my publicly-auditable identity here: https://keybase.io/subnetzero

==================================================================
