---
categories: ["iridium"]
title: "This Week in Iridium - #4"
date: 2018-08-27T08:27:21-07:00
description: Summary of what happened in the fourth week
draft: false
type: project-tutorial
series: iridium-vm
og_title: "This Week in Iridium - #4"
og_type: "article"
og_article_section: "Technology"
og_article_tags: ["rust", "compiler", "programming"]
---

== VM Changes
* Latest version in GitLab fixes all clippy complaints
* Two-pass assembler is working fairly well, planning to start adding more features directly to the VM now

== Website Changes
* Redesigned website to hopefully make navigation easier
* Made some spaces for non-Iridium projects
* Old links should still work fine, and will take you to the page at the new URL structure

== Tutorials Added
* Added tutorials up through #16

== Other Notes
Apologies for the slow updates this week. I spent quite a bit of time on the website. As you may have guessed, I'm not good with UI/UX...
