---
categories: ["iridium"]
title: "This Week in Iridium - #7"
date: 2018-09-24T14:00:00-07:00
description: Summary of what happened in the seventh week
draft: false
type: project-tutorial
series: iridium-vm
og_title: "This Week in Iridium - #7"
og_type: "article"
og_article_section: "Technology"
og_article_tags: ["rust", "compiler", "programming"]
---
== Week 7
Hello!

Below is a list of changes made this week in https://blog.subnetzero.io/categories/iridium/[Iridium] and https://blog.subnetzero.io/categories/palladium/[Palladium]

The tutorials this week start here: https://blog.subnetzero.io/post/building-language-vm-part-24/

Changes:

* Multi-user remote access (with no security) to REPLs. Thrussh proved unworkable, so we did straight TCP and will add encryption and AAA on top
* Added in support for f64 in Iridium ASM. f64 instructions have the suffix `F64`.
* Extended Palladium to parse nested expressions. `(3*4)-1` for example. It also now handles f64s.
