---
categories: ["iridium"]
title: "This Week in Iridium - #3"
date: 2018-08-27T08:27:21-07:00
description: Summary of what happened in the third week
draft: false
aliases:
    - /post/this-week-in-iridium-03/
    - /post/this-week-in-iridium-03/index.html
type: project-tutorial
series: iridium-vm
og_title: "This Week in Iridium - #3"
og_type: "article"
og_article_section: "Technology"
og_article_tags: ["rust", "compiler", "programming"]
---

== VM Changes
* Fixed a bug with some of the opcodes being interpretered as an incorrect one that was one Opcode higher numerically
* Fixed a bug with the `run()` function that was causing the execution loop to exit early
* Added in basic heap memory
* Started on an Assembler; added directives and segments to the assembler
* Started defining a bytecode format

== Website Changes
* Every tutorial series now has a `Previous` and `Next` link at the top and bottom to make it easier to navigate
* Still thinking about more navigation improvements to do

== Tutorials Added
* Added tutorials up through #14. They cover strings, the heap, assembler labels, and more about the assembler
