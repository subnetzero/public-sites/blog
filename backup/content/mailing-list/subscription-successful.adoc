+++
categories = ["mailing list"]
date = "2018-08-22T00:18:47-07:00"
description = "Tells user when they subscribe to the mailing list"
slug = "subscription-successful"
title = "Mailing List Subscription Successful"
draft = false
author = "Fletcher Haynes <fletcher@subnetzero.io>"
layout = "subscription-successful"
+++


Yay! You subscribed to the mailing list! I do not send out a lot of e-mails; less than one per week. Each e-mail will have a unsubscribe link you can use to remove yourself.
