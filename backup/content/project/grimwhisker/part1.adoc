+++
categories = ["infrastructure", "data", "tutorial", "grimwhisker", "pipeline"]
date = "2018-09-06T17:23:18-07:00"
description = "Prelude to a series of tutorials that go through building a data pipeline."
tags = ["infrastructure", "data", "tutorial", "grimwhisker", "pipeline"]
title = "Grimwhisker Design"
draft = true
author = "Fletcher Haynes <fletcher@subnetzero.io>"
series = "grimwhisker"
series_name = "grimwhisker"
part = 1
+++

== Intro to the Project
Hi everyone! This tutorial is part of a series meant to show how to build large scale systems and applications from the ground up. Rather than focusing on a specific technology, we build a product.

This specific tutorial focuses on building a product for large scale (multi-Gbps) data ingestion, storage, and analytics system. All the tech we use will be open-source. The rest of this post is discussing the product we'll build and its requirements. Heeeeere we go!

== Our Product
Let's say that extensive market research indicates that there is strong demand for a service that can accept data from devices anywhere in the world, store it, retrieve it, or perform analytics on it

=== The Name
The most important part: choosing a name! I'm going to call it Panopticon, which means "all-seeing".

=== Requirements
We'll skip the tedious design meetings, arguments with PMs, TPMs, CPAs, etc. and I'll list the requirements for the product:

. Able to accept data from anywhere in the world
  * Must support JSON via HTTP
. Customers should be able to define a schema for a message, and choose to drop messages that don't conform
. Customers should be able to specify which field in a schema should be encrypted
. Customers should be able to name each schema
. Must be able to rate-limit incoming topics
. High-quality libraries for C#, Python, Go
. Tenants get a unique ID
. Tenants can have multiple people
. Tenants can create "topics". A topic must have a schema.
. Messages can be POSTed to the API. Assuming a valid topic and that it matches the schema, its stored and made available for access

=== Priorities

. Reliability (We want 99.9% uptime. See https://en.wikipedia.org/wiki/High_availability[here] if you aren't familiar with that.)
. Resiliency
. Performance

[IMPORTANT]
A part I'm skipping here is talking about PII and GDPR. Don't worry, we'll get to it, but it merits its own post.

== Breaking Down Problems
One of my favorite problem-solving techniques is...er, well, I'm not sure of the official name, but its breaking down problems into smaller problems. Usually on a piece of paper or a whiteboard. I'm going to follow that model for this tutorial series; we'll break down the problem into ever-smaller pieces and work on them one by one.

=== Problem Statement
Problem: "I do not have a globally-available key-value store that is highly resilient."

=== Breakdown: Round 1
Here's a rough list of macro-level components we'll need:

. Data Ingest
. Storage
. Processing
. Authentication
. Customer portal (schema editor, set limits, see bill, etc)

=== Breakdown: Round 2
A data ingestion system is going to need:

. 1+ API servers
. Some way to ship to storage
...

And you just keep going until you get to the smallest size of task that you need. My rule of thumb is that an average developer should be able to implement it within 2 weeks.

== Common Architectures

As in code, there are sets of common tools used with most patterns, as well as more specific patterns in infrastructure that pop up over and over.

=== Universal Components
Unless you have a good reason, all architectures should contain these:

. Monitoring
. Logging
. Metrics

It may not be in the same form if you are dealing with, say, embedded devices; but they should be there in some form. Let's move on to our first pattern...
=== Basic API
These are an _application_ of some sort that is meant to accept requests, almost always over HTTP, do something, and respond. From a components perspective, these usually have:

. Application Servers
. Database Servers
. Load Balancers
