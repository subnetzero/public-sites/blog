+++
categories = ["infrastructure", "data", "tutorial", "grimwhisker", "pipeline"]
date = "2018-09-07T13:11:18-07:00"
description = "Builds the edge ingestion API"
tags = ["infrastructure", "data", "tutorial", "grimwhisker", "pipeline"]
title = "Edge API Service"
draft = true
author = "Fletcher Haynes <fletcher@subnetzero.io>"
series = "grimwhisker"
series_name = "grimwhisker"
part = 2
+++

== Introduction
Hello! Welcome to part 2 of the Grimwhisker Data Pipeline Project. In this tutorial, we'll build a basic API server that lives at the edge of our system and is the entry point for data.

== Edge API
You'll often hear the term "edge". Network edge, application edge, and more. 
