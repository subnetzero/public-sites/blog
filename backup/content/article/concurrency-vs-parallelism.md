---
categories: ["development", "performance", "programming", "parallelism", "concurrency", "article"]
title: "Concurrency vs Parallelism"
date: 2018-09-02T21:39:52-07:00
draft: false
slug: "concurrency-vs-parallelism"
author:
  first_name: Fletcher
  last_name: Haynes
  email: fletcher@subnetzero.io
summary: "Discusses the differences between parallelism and concurrency."
difficulty: "Intermediate"
---

# Summary
Hey everyone! This is a short article on concurrency, parallelism, and the differences between the two.

# Performance and Concurrency
What is performance? Is it how fast a machine can calcuate prime numbers? Is it how many things a machine can do at one time? As always, it depends on the workload.

As a rule of thumb, in terms of _serial execution_, there is an inverse relationship between language dynamism and performance. The more dynamic a language is, the slower it will perform. C is very fast, but good luck getting it to do:

```ruby
3.times { puts "This isn't confusing at all!" }
```

## Concurrency and Parallelism
The difference between these two things is important to know, but its often confusing to people.

> Concurrency is _not_ parallelism. Concurrency means that more than one thing happens in some time slice. Parallelism means two things happening _simultaneously_. Parallelism is one way to achieve concurrency, but *not the only way*.

Let's say we have two functions, `unicorn()` and `prance()`. In a non-parallel, non-concurrent system, execution looks like this:

`unicorn()` -> `unicorn()` does work -> `unicorn()` finishes -> `prance()` -> `prance()` does work -> `prance()` finishes

In a concurrent system, it might look like this:

`unicorn()` -> `unicorn()` does work -> `prance()` -> `prance()` does work -> `unicorn()` finishes -> `prance()` finishes
See how unicorn started, then prance started?

In a parallel system, it would look like this:
`unicorn()` -> `unicorn()` does work -> `unicorn()` finishes
`prance()` -> `prance()` does work -> `prance()` finishes

Think of each line as code running on a processor. In the concurrent example, one CPU still does all the work. In the parallel example, two processors do all the work.

> The more you can ~parallel~ ~parralell~ screw it, the more you can subdivide your workload into independent pieces, the more you can leverage multiple cores to increase performance.

## Notes on OS processes, OS threads, and green/micro/lightweight threads
Another concept that often engenders confusion is the differences between processes, threads, microthreads, greenthreads, etc etc. These are important to know about and understand how they impact performance.

### OS Processes
Any application lives as some sort of code on some sort of storage somewhere. When you open an application, that code is read from its storage and into the RAM of the machine to begin running. This is all done by the operating system, and when it has finished, you have a _process_ with a unique ID. When you run a python program, a process containing the Python virtual machine is created, for example.

You can think of the OS processe as the most heavy-weight of the three and slowest to create. The key difference to remember is:

*_Processes do not share memory by default_*

### Threads
_Threads_ are created by the operating system, belong to a _Process_ and are lighter weight. In contrast to processes:

*_Threads do share memory by default_*

### Greenthreads, Microthreads, et article
These are often implemeneted and managed by the language runtime and are very light weight. The key thing to remember is:

*_The operating system cannot even see these threads_*

### Heavyweight? Lightweight? What?
This refers to the amount of resources (RAM, CPU) it takes to create and manage. Processes require the most, green threads the least.
