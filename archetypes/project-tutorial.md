+++
title = "{{ replace .TranslationBaseName "-" " " | title }}"
date = "{{ .Date }}"
author = "Fletcher Haynes <fletcher@subnetzero.io>"
description = ""
slug = ""
tags = ["tutorial", "project"]
categories = ["tutorial", "project"]
draft = "true"
series = "iridium-vm"
type = "project-tutorial"
vcs_url = ""
semver.major = ""
semver.minor = ""
semver.ptch = ""
+++
