all: clean build publish clean

prepare: clean build compress

clean:
	rm -rf public/

build:
	HUGO_ENV="production" hugo --minify

compress:
	find public -type f -name '*.html' -o -name '*.js' -o -name '*.css' -o -name '*.xml' -o -name '*.svg' | xargs gzip --best -k -v -f

publish:
	gsutil -h "Cache-Control:public,max-age=86400" -m rsync -d -R public gs://blog.subnetzero.io
  gsutil acl -m set -R public-read gs://blog.subnetzero.io/

yarn:
	cd themes/hugo-nuo; yarn clean; yarn build; yarn imagemin
